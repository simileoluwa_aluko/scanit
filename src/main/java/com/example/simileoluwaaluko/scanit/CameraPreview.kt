package com.example.simileoluwaaluko.scanit

import android.content.Context
import android.hardware.Camera
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView

class CameraPreview(context: Context, private val camera : Camera) : SurfaceView(context), SurfaceHolder.Callback {
    private val tag = "CameraPreview"
    private val mHolder : SurfaceHolder = holder
    init {
        mHolder.addCallback(this)
    }

    override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
        //preview surface shouldnt change
    }

    override fun surfaceDestroyed(p0: SurfaceHolder?) {
        //empty. taken care of in activity
    }

    override fun surfaceCreated(p0: SurfaceHolder?) {
      try {
          camera.setPreviewDisplay(mHolder)
          camera.setDisplayOrientation(90)
          setCamFocusMode(camera)
          camera.startPreview()
      }catch (e : Exception){
          Log.e(tag, "error starting camera preview ${e.message}")
      }
    }

    private fun setCamFocusMode(camera : Camera){
        val parameters = camera.parameters
        val focusModes = parameters.supportedFocusModes
        if(focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            parameters.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
        else if (focusModes.contains(Camera.Parameters.FLASH_MODE_AUTO))
            parameters.focusMode = Camera.Parameters.FOCUS_MODE_AUTO

        camera.parameters = parameters
    }
}