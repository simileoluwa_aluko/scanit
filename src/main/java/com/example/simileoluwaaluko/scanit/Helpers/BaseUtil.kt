package com.example.simileoluwaaluko.scanit.Helpers

import android.content.Context
import android.os.Build
import android.os.Environment
import android.view.View
import com.example.simileoluwaaluko.scanit.R
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by simileoluwaaluko on 13/05/2018.
 */
object BaseUtil {
    const val CAMERA_PERMISSION_REQUEST_CODE = 100
    enum class PermissionResult{PERMISSION_GRANTED, PERMISSION_DENIED, REQUEST_PERMISSION}
    private val sNextGeneratedId = AtomicInteger(1)

    fun createImageFile(context : Context): File {
        val timeStamp = SimpleDateFormat("dd_MM_yy_HHmmss").format(Date())
        val imageFileName = "JPEG_$timeStamp"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return File.createTempFile(imageFileName, context.getString(R.string.jpg_extension), storageDir)
    }

    fun generateId() : Int{
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return generateViewId()
        }
        return View.generateViewId()
    }

    /**
     * Generate a value suitable for use in [.setId].
     * This value will not collide with ID values generated at build time by aapt for R.id.
     *
     * @return a generated ID value
     */
    private fun generateViewId(): Int {
        while (true) {
            val result = sNextGeneratedId.get()
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            var newValue = result + 1
            if (newValue > 0x00FFFFFF) newValue = 1 // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result
            }
        }
    }
}