package com.example.simileoluwaaluko.scanit

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Camera
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.view.View
import android.widget.TextView
import com.example.simileoluwaaluko.scanit.Helpers.BaseUtil
import com.example.simileoluwaaluko.scanit.Helpers.BaseUtil.CAMERA_PERMISSION_REQUEST_CODE
import com.example.simileoluwaaluko.scanit.Helpers.BaseUtil.generateId
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import java.io.File

class MainActivity : AppCompatActivity() {

    private lateinit var photoFile : File
    private var camera : Camera? = null
    private val vm = MainActivityViewModel(this)


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == BaseUtil.CAMERA_PERMISSION_REQUEST_CODE && resultCode == RESULT_OK){
            photo_uri.text = photoFile.absolutePath
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            CAMERA_PERMISSION_REQUEST_CODE -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    showCameraPreview()
                    showSnackBar(getString(R.string.permission_granted), null)
                }else{
                    showSnackBar(getString(R.string.permission_denied), getString(R.string.grant_permission))
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val permissionResult = vm.checkCameraPermission(this)

        when(permissionResult){
            BaseUtil.PermissionResult.PERMISSION_GRANTED -> showCameraPreview()
            BaseUtil.PermissionResult.PERMISSION_DENIED ->
                showSnackBar(getString(R.string.permssion_snackbar_message), getString(R.string.grant_permission))
            BaseUtil.PermissionResult.REQUEST_PERMISSION ->
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                    BaseUtil.CAMERA_PERMISSION_REQUEST_CODE)
        }
        setUpListeners()
    }

    private fun showSnackBar(message : String, action : String?){
        if(action != null){
            val snackBar = Snackbar.make(main_layout, message, Snackbar.LENGTH_INDEFINITE)
                    .setAction(action, {
                        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                                BaseUtil.CAMERA_PERMISSION_REQUEST_CODE)
                    })
            snackBar.show()
            return
        }
        val snackBarWithoutAction = Snackbar.make(main_layout, message, Snackbar.LENGTH_LONG)
        snackBarWithoutAction.show()
    }

    private fun setUpListeners(){

        take_picture_fab.setOnClickListener {
            val photoIntentAndFile = vm.createPhotoIntentAndFile()
            if(photoIntentAndFile.photoFile != null && photoIntentAndFile.photoIntent != null){
                photoFile = photoIntentAndFile.photoFile
                startActivityForResult(photoIntentAndFile.photoIntent, CAMERA_PERMISSION_REQUEST_CODE)
            }else{
                showSnackBar(getString(R.string.camera_is_unavailable), null)
            }
        }
    }

    private fun showCameraPreview(){
        val hasCamera = vm.checkCameraHardware()

        if(hasCamera){
            camera = vm.getCameraInstance()
            if(camera != null){
                val preview = CameraPreview(this, camera!!)
                camera_preview_layout.addView(preview)
                preview.bringToFront()
            }else{
                val textView = TextView(this)
                textView.text = getString(R.string.camera_preview_unavailable)
                camera_preview_layout.addView(textView)
            }

        } else {
            showSnackBar(getString(R.string.device_doesnt_have_a_camera), null)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(camera != null){
            camera!!.stopPreview()
            camera!!.release()
        }
    }

    override fun onResume() {
        super.onResume()
        showCameraPreview()
    }
}