package com.example.simileoluwaaluko.scanit

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Camera
import android.net.Uri
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.util.Log
import com.example.simileoluwaaluko.scanit.Helpers.BaseUtil
import java.io.File
import java.io.IOException


class MainActivityViewModel(private val context: Context) : ViewModel() {
    private val tag = "mainActivityViewModel"

    fun checkCameraHardware() = context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)

    fun getCameraInstance(): Camera? {
        var camera : Camera? = null
        try {
            camera = Camera.open()
        }catch (e : Exception){
            Log.e(tag, context.getString(R.string.camera_is_not_available))
        }
        return camera
    }

    fun checkCameraPermission(activity:Activity): BaseUtil.PermissionResult {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED) {
            return if(ActivityCompat.shouldShowRequestPermissionRationale(activity,Manifest.permission.CAMERA)){
                BaseUtil.PermissionResult.PERMISSION_DENIED
            }else{
                BaseUtil.PermissionResult.REQUEST_PERMISSION
            }
        }
        return BaseUtil.PermissionResult.PERMISSION_GRANTED
    }

    class PhotoIntentAndFile(val photoIntent : Intent?, val photoFile : File?)

    fun createPhotoIntentAndFile() : PhotoIntentAndFile{
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        var photoFile : File? = null
        var photoUri : Uri? = null

        if(takePictureIntent.resolveActivity(context.packageManager) != null){
            try {
                photoFile = BaseUtil.createImageFile(context)
            }catch (e : IOException){
                e.printStackTrace()
            }

            if(photoFile != null){
                photoUri = FileProvider.getUriForFile(context,
                        context.getString(R.string.image_provider_authority),
                        photoFile)
            }else{
                return PhotoIntentAndFile(null, null)
            }
        }
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
        return PhotoIntentAndFile(takePictureIntent, photoFile!!)
    }
}